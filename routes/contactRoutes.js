const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");  

const {
  getContacts,
  getContactById,
  createContact,
  updateContact,
  deleteContact,
} = require("../controllers/contactController");

router.get("/3", (req, res) => {
  res.send(" le contact 3 est protégé");
});



//router.use(authMiddleware.authenticate()); 
router.get("/", getContacts);
router.get("/:id", getContactById);
router.post("/", createContact);
router.put("/:id", updateContact);
router.delete("/:id", deleteContact);

module.exports = router;
