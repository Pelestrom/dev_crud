const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");  

const {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
} = require("../controllers/userController");

 
//router.use(authMiddleware.authenticate());  

router.get("/1", (req, res) => {
  res.send("le user 1 est protégé");
});

router.get("/", getUsers);
router.get("/:id", getUserById);
router.post("/", createUser);
router.put("/:id", updateUser);
router.delete("/:id", deleteUser);

module.exports = router;
