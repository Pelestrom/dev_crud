const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");

const connection = require("../config/db");

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
    },
    async (email, password, done) => {
      try {
        connection.query(
          "SELECT * FROM user WHERE email = ?",
          [email],
          async (err, rows) => {
            if (err) {
              return done(err);
            }
            if (!rows.length) {
              return done(null, false, { message: "Email incorrect" });
            }

            const user = rows[0];
            const validPassword = await bcrypt.compare(password, user.password);

            if (!validPassword) {
              return done(null, false, { message: "Mot de passe incorrect" });
            }

            return done(null, user);
          }
        );
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  connection.query("SELECT * FROM user WHERE id = ?", [id], (err, rows) => {
    done(err, rows[0]);
  });
});

module.exports = passport;
