const connection = require("../config/db");

// Fonction pour récupérer tous les utilisateurs
const getUsers = (req, res) => {
  connection.query("SELECT * FROM user", (err, rows) => {
    if (err) {
      console.log("Erreur lors de la récupération des utilisateurs : ", err);
      return res
        .status(500)
        .json({ error: "Erreur lors de la récupération des utilisateurs" });
    }
    res.json(rows);
  });
};

// Fonction pour récupérer un utilisateur par son ID
const getUserById = (req, res) => {
  const { id } = req.params;
  connection.query("SELECT * FROM user WHERE id = ?", [id], (err, rows) => {
    if (err) {
      console.log("Erreur lors de la récupération de l'utilisateur : ", err);
      return res
        .status(500)
        .json({ error: "Erreur lors de la récupération de l'utilisateur" });
    }
    if (rows.length === 0) {
      return res.status(404).json({ message: "Utilisateur non trouvé" });
    }
    res.json(rows[0]);
  });
};

// Fonction pour créer un nouvel utilisateur
const createUser = (req, res) => {
  const { nom, email, password } = req.body;
  connection.query(
    "INSERT INTO user (nom, email, password) VALUES (?, ?, ?)",
    [nom, email, password],
    (err, result) => {
      if (err) {
        console.log("Erreur lors de la création de l'utilisateur : ", err);
        return res
          .status(500)
          .json({ error: "Erreur lors de la création de l'utilisateur" });
      }
      res.json({ message: "Utilisateur créé avec succès" });
    }
  );
};

// Fonction pour mettre à jour un utilisateur
const updateUser = (req, res) => {
  const { id } = req.params;
  const { nom, email, password } = req.body;
  connection.query(
    "UPDATE user SET nom = ?, email = ?, password = ? WHERE id = ?",
    [nom, email, password, id],
    (err, result) => {
      if (err) {
        console.log("Erreur lors de la mise à jour de l'utilisateur : ", err);
        return res
          .status(500)
          .json({ error: "Erreur lors de la mise à jour de l'utilisateur" });
      }
      res.json({ message: "Utilisateur mis à jour avec succès" });
    }
  );
};

// Fonction pour supprimer un utilisateur
const deleteUser = (req, res) => {
  const { id } = req.params;
  connection.query("DELETE FROM user WHERE id = ?", [id], (err, result) => {
    if (err) {
      console.log("Erreur lors de la suppression de l'utilisateur : ", err);
      return res
        .status(500)
        .json({ error: "Erreur lors de la suppression de l'utilisateur" });
    }
    res.json({ message: "Utilisateur supprimé avec succès" });
  });
};

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
