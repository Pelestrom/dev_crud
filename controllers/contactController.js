const connection = require("../config/db");

// Fonction pour récupérer tous les contacts
const getContacts = (req, res) => {
  connection.query("SELECT * FROM contact", (err, rows) => {
    if (err) {
      console.log("Erreur lors de la récupération des contacts : ", err);
      return res
        .status(500)
        .json({ error: "Erreur lors de la récupération des contacts" });
    }
    res.json(rows);
  });
};

// Fonction pour récupérer un contact par son ID
const getContactById = (req, res) => {
  const { id } = req.params;
  connection.query("SELECT * FROM contact WHERE id = ?", [id], (err, rows) => {
    if (err) {
      console.log("Erreur lors de la récupération du contact : ", err);
      return res
        .status(500)
        .json({ error: "Erreur lors de la récupération du contact" });
    }
    if (rows.length === 0) {
      return res.status(404).json({ message: "Contact non trouvé" });
    }
    res.json(rows[0]);
  });
};

// Fonction pour créer un nouveau contact
const createContact = (req, res) => {
  const { user_id, nom,tel } = req.body;
  connection.query(
    "INSERT INTO contact (user_id, nom,tel) VALUES ( ?, ?, ?)",
    [user_id, nom,tel],
    (err, result) => {
      if (err) {
        console.log("Erreur lors de la création du contact : ", err);
        return res
          .status(500)
          .json({ error: "Erreur lors de la création du contact" });
      }
      res.json({ message: "Contact créé avec succès" });
    }
  );
};

// Fonction pour mettre à jour un contact
const updateContact = (req, res) => {
  const { id } = req.params;
  const { user_id, nom,tel} = req.body;
  connection.query(
    "UPDATE contact SET user_id = ?, nom = ?, tel = ? WHERE id = ?",
    [user_id, nom,tel, id],
    (err, result) => {
      if (err) {
        console.log("Erreur lors de la mise à jour du contact : ", err);
        return res
          .status(500)
          .json({ error: "Erreur lors de la mise à jour du contact" });
      }
      res.json({ message: "Contact mis à jour avec succès" });
    }
  );
};

// Fonction pour supprimer un contact
const deleteContact = (req, res) => {
  const { id } = req.params;
  connection.query("DELETE FROM contact WHERE id = ?", [id], (err, result) => {
    if (err) {
      console.log("Erreur lors de la suppression du contact : ", err);
      return res
        .status(500)
        .json({ error: "Erreur lors de la suppression du contact" });
    }
    res.json({ message: "Contact supprimé avec succès" });
  });
};

module.exports = {
  getContacts,
  getContactById,
  createContact,
  updateContact,
  deleteContact,
};
