const express = require("express");
const bodyParser = require("body-parser");
const session = require("express-session");
const passport = require("./middlewares/authMiddleware"); // Importez Passport
const userRouter = require("./routes/userRoutes");
const contactRouter = require("./routes/contactRoutes");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({ secret: "secret", resave: false, saveUninitialized: false }));
app.use(passport.initialize()); // Initialisation de Passport

app.use("/users", userRouter);
app.use("/contacts", contactRouter);

app.listen(3000, () => {
  console.log("Server is running on port 3000");
});
